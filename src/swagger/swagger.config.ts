import { SwaggerConfig } from "./swagger.interface";

export const SWAGGER_CONFIG: SwaggerConfig = {
    title: 'Login ',
    description: 'Templete',
    version: '1.0',
    tags: ['Templete']
}