import { Body, CacheInterceptor, CacheKey, CacheTTL, Controller, Delete, Get, HttpException, HttpStatus, Param, Post, Put, UseFilters, UseInterceptors, UsePipes } from '@nestjs/common';
import { ApiOkResponse, ApiForbiddenResponse, ApiTags, ApiCreatedResponse } from '@nestjs/swagger';
import { BenchmarkInterceptor } from '../interceptors/benchmark.interceptors';
import { User } from './entity/user.entity';
import { UserService } from './user.service';


@ApiTags('user')
@Controller('user')
@UseInterceptors(CacheInterceptor,BenchmarkInterceptor)
export class UserController {

  constructor(private readonly userService: UserService) {}

  @Get()
  @CacheKey('AllUsers')
  @CacheTTL(50)
  @ApiOkResponse({
    description: ' The resource list has been successfully returned.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  getUser(): Promise<User[]> {
    return this.userService.getUser();
  }

  @Get('/id/:id')
  @CacheTTL(20)
  @ApiOkResponse({
    description: ' The resource list has been successfully returned.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  getUserById(@Param('id') id: number): Promise<User> {
    return this.userService.getUserById(id);
  }


  @Post()
  @ApiCreatedResponse({
    description: ' The resource list has been successfully created.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  postUser(@Body() user: User): Promise<User> {
    return this.userService.addUser(user);
  }


  @Put()
  @ApiOkResponse({
    description: ' The resource list has been successfully updated.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  updateUser(@Body('id') id: number,@Body() user: User): Promise<User> {
    return this.userService.updateUser(id,user)
  }

  @Delete('/:id')
  @ApiOkResponse({
    description: ' The resource list has been successfully removed.',
  })
  @ApiForbiddenResponse({ description: 'Forbidden.' })
  deleteUserById(@Param('id') id: number){
    return this.userService.deleteUserById(id);
  }

}