import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './entity/user.entity';
 
 
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}
 
  getUser(): Promise<User[]> {
    return this.userRepository.find();
  }
 
  getUserById(id: number): Promise<User> {
    return this.userRepository.findOne({id});
  }
 
  addUser(user: User): Promise<User> {
    return this.userRepository.save(user);
  }
  
  async updateUser(id: number, user:User):Promise<User>{
    await this.userRepository.update(id, user);
     return this.userRepository.findOne({id});
  }
 
  deleteUserById(id: number) {
   return  this.userRepository.delete({id});
  }
  
}