import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User {
  
  @ApiProperty({
    type: Number,
    description: 'The id of the Advanced user',
    default: '',
})
@PrimaryGeneratedColumn()
public id: number;

@ApiProperty({
  type: String,
  description: 'The first name of the Advanced user',
  default: '',
})
@Column()
public firstName: string;

@ApiProperty({
  type: String,
  description: 'The last name of the Advanced user',
  default: '',
})
@Column()
public lastName: string;

@ApiProperty({
  type: String,
  description: 'The dob of the Advanced user',
  default: '',
})
@Column()
public dob: string;

@ApiProperty({
  type: Number,
  description: 'The age of the Advanced user',
  default: '',
})
@Column()
public age: number;

@ApiProperty({
  type: String,
  description: 'The email of the Advanced user',
  default: '',
})
@Column()
public email: string;

@ApiProperty({
  type: Number,
  description: 'The phone number of the Advanced user',
  default: '',
})
@Column()
public phoneNumber: number;

@ApiProperty({
  type: String,
  description: 'The address of the Advanced user',
  default: '',
})
@Column()
public address: string;

@ApiProperty({
  type: String,
  description: 'The state of the Advanced user',
  default: '',
})
@Column()
public state: string;

@ApiProperty({
  type: String,
  description: 'The country of the Advanced user',
  default: '',
})
@Column()
public country: string;

@ApiProperty({
  type: Number,
  description: 'The pincode of the Advanced user',
  default: '',
})
@Column()
public pinCode: number;


}