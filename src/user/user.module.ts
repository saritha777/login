import { CacheModule, MiddlewareConsumer, Module, NestModule, RequestMethod } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuditMiddleware } from "../middlewares/audit.middleware";
import { User } from "./entity/user.entity";
import { UserController } from "./user.controller";
import { UserService } from "./user.service";

@Module({
    imports: [ TypeOrmModule.forFeature([User]),
        CacheModule.register({
            ttl:5, //seconds
            max:100, //maximum number of items in cache
        })
    ],
    controllers: [ UserController ],
    providers: [ UserService ]
})
export class UserModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer.apply(AuditMiddleware).forRoutes( { path:'user/*', method : RequestMethod.DELETE})
    }
    
}